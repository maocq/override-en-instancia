package com.maocq.overrride;

import java.util.ArrayList;
import java.util.List;

public class Main {

	public static String variable;

	public static void main(String[] args) {
		Human hombre = new Human("John");
		hombre.hacerAlgo(new Do() {
			@Override
			public void doSomething(String name) {
				System.out.println(name + " no hara nada");
				variable = "No hizo nada";
			}
		});
		System.out.println(variable);

		/**************************************************/
		
		Human persona = new Human("Mauricio");
		persona.hacerAlgo(new Do() {
			@Override
			public void doSomething(String nombre) {
				List<String> tareas = new ArrayList<String>();
				tareas.add("Caminar");
				tareas.add("Correr");

				System.out.println("\n" + nombre + ":");
				for (String tarea : tareas) {
					System.out.println(tarea);
				}
			}
		});

	}

}
