package com.maocq.overrride;

public class Human {

	public String name;

	public Human(String name) {
		this.name = name;
	}

	/**
	 * 
	 * @param hacer interface DO
	 */
	public void hacerAlgo(Do hacer) {
		hacer.doSomething(this.name);
	}

}
